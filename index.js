const btn = document.querySelector('button');
const text = document.querySelector('textarea');

// addEventListner
btn.addEventListener('click', () => {
    let utterance = new SpeechSynthesisUtterance(text.value);
    speechSynthesis.speak(utterance);
})